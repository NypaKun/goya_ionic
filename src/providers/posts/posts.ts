import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../Api';

@Injectable()
export class PostsProvider {

	private url : string = this.API.GetURL();
	public posts : any;
	public urlImg : string = "/image"
	constructor(public API : Api, public http: HttpClient) {
		console.log('Hello PostsProvider Provider');
	}

	query(idCategory : number, idCity : number){
		return new Promise((resolve, reject) => {
	      this.http.get(this.url +"/getPostsByCategory/"+idCategory+"/"+idCity).subscribe((res : [any]) => {
	        this.posts = res;
	        resolve();
	      }, (err) => {
	        console.error(err);
	        reject();
	      });
	    });
	}

}
