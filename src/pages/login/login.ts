import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ClientProvider } from '../../providers/client/client';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { WelcomePage } from '../welcome/welcome';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	formRegister : FormGroup;

	constructor(
	  	public navCtrl: NavController,
	  	public client: ClientProvider,
	  	public formBuilder: FormBuilder,
	  	public alert: AlertController
	  	) {																																																													
		this.formRegister = this.createMyForm();
	}

  	ionViewDidLoad() {
    																																			
  	}

  	private createMyForm(){
		return this.formBuilder.group({
			name: ['', Validators.required],
			email: ['', Validators.required],
			age: ['', Validators.required],
		});
	}

	saveData(){
		this.client.requestCreate(this.formRegister.value.email, this.formRegister.value.name, this.formRegister.value.age)
		.then( () => {

			this.alert.create({
			    title: 'Genial',
			    subTitle: 'Gracias por registrarte.',
			    buttons: [{
			    	text: 'Ok',
			    	handler: () => {
			    		this.navCtrl.push(WelcomePage);
			    	}
			    }]
			}).present();
		})
		.catch(err => {
			this.alert.create({
			    title: 'Error',
			    subTitle: 'Ha ocurrido un error, verifique su conexion a internet o vuelva a intentarlo mas tarde.',
			    buttons: ['Ok']
			}).present();
		})
	}

}
