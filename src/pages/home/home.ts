import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { ViewPostPage } from '../view-post/view-post';
import { PostsProvider } from '../../providers/posts/posts';
import { CityProvider } from '../../providers/city/city';
import { Api } from '../../providers/Api';
import { CardsProvider } from '../../providers/cards/cards';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	loading : boolean = true;

	constructor(
  			public navCtrl: NavController,
  			public postProv: PostsProvider,
  			public cityProv: CityProvider,
  			public alertCtrl: AlertController,
  			public API : Api,
        public CardProvider: CardsProvider
  	) {
 		this.postProv.query(0, this.cityProv.selected)
 		.then( () => {
 			this.loading = false;
 		})
 		.catch( () => {
 			this.loading = true;
 			this.alertCtrl.create({
			    title: 'Error',
			    subTitle: 'Ha ocurrido un error, verifique su conexion a internet o vuelva a intentarlo mas tarde',
			    buttons: ['Ok']
			  }).present();
 		});
  	}

  	onClickCard(post : object){
  		this.navCtrl.push(ViewPostPage, {post:post});
  	}

    setType(t:number){
      this.CardProvider.setType(t);
    }

}
