import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapDirectionsPage } from './map-directions';

@NgModule({
  declarations: [
    MapDirectionsPage,
  ],
  imports: [
    IonicPageModule.forChild(MapDirectionsPage),
  ],
})
export class MapDirectionsPageModule {}
